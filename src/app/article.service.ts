import { Article } from "./article";
export interface IAricleService {
  getArticles(): Promise<Article[]>;
  removeArticle(id: number): any;
  modifyArticle(article: Article): any;
}
